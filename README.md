# congatec BSP for Android 10 and Apollo Lake platform products
*This BSP provides support for congatec Apollo Lake platform based products as conga-TA50, conga-QA50, conga-MA50, conga-SA50 and conga-PA50.*

# I. Build Host Requirements
- RAM :  **16 GB (for 8 cores build)**
- HDD :  **250 GB**
- OS :  **Ubuntu 18.04.4 LTS**

[ Note: *It is highly recommended to keep the build in virtual machine. This 
helps to maintain host environment consistent and reliable.* ]

# II. Packages
Please make sure you have installed following packages.

## Build environment packages :
```
sudo apt install git-core gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev libgl1-mesa-dev libxml2-utils xsltproc unzip openjdk-8-jdk git ccache automake lzop bison gperf build-essential zip curl zlib1g-dev g++-multilib python-networkx libxml2-utils bzip2 libbz2-dev libbz2-1.0 libghc-bzlib-dev squashfs-tools pngcrush schedtool dpkg-dev liblz4-tool make optipng maven libssl-dev bc bsdmainutils gettext python-mako libelf-dev sbsigntool dosfstools mtools efitools python-pystache git-lfs efitools libkmod-dev
```

## Recommended packages :
```
sudo apt install net-tools screen openssh-server
```

## Check Java version :
```
java -version
openjdk version "11.0.6" 2020-01-14`
OpenJDK Runtime Environment (build 11.0.6+10-post-Ubuntu-1ubuntu118.04.1)`
OpenJDK 64-Bit Server VM (build 11.0.6+10-post-Ubuntu-1ubuntu118.04.1, mixed mode, sharing)`
```

# III. Sources
## Make sure you have name and email configured for the git :
```
git config --global user.email "your@email.com"
git config --global user.name "Your Name"
```

[ Note: *Following instructions will expect user honza. Therefore this can 
reflect in paths and file names.* ]

## Obtain the repo tool :
```
mkdir ~/.bin
cd ~/.bin

sh -c "curl https://storage.googleapis.com/git-repo-downloads/repo > repo"
sudo chmod a+x repo
export PATH=$(pwd):$PATH
echo 'export PATH='$(pwd)':$PATH' >> ~/.profile
```

## Get the project source code :
```
mkdir ~/celadon
cd ~/celadon
```

## Two versions of are available - stable and master. The stable version is recommended since master keeps the latest development state.
### b. stable ww201941_B (recommended) :
```
repo init -u https://git.congatec.com/android-ia/platform_manifest -b ww201941_B.apl -m stable-build/ww201941_B.xml
```

### a. master :
```
repo init -u https://git.congatec.com/android-ia/platform_manifest -b ww201941_B.apl
```

## Download the source code :
```
repo sync -c -j8 --force-sync
```

# IV. Trusty
The AOSP project provides highly secure parallel OS - Trusty. This handle all secure AOSP tasks. Unfortunately this requires TPM and BIOS features precisely configured.

## It helps a lot for development to disable the Trusty :
```
nano device/intel/project-celadon/celadon_tablet/mixins.spec
  -   boot-arch: project-celadon(bootloader_policy=0x0,bootloader_len=60,magic_key_timeout=80,assume_bios_secure_boot=true,rpmb_simulate=true,disk_encryption=false,file_encryption=true,ignore_not_applicable_reset=true,self_usb_device_mode_protocol=true,watchdog_parameters=10 30,system_partition_size=3584)
  +   boot-arch: project-celadon(bootloader_policy=0x0,bootloader_len=60,magic_key_timeout=80,assume_bios_secure_boot=true,rpmb_simulate=true,disk_encryption=false,file_encryption=true,ignore_not_applicable_reset=true,self_usb_device_mode_protocol=true,watchdog_parameters=10 30,system_partition_size=3584,tos_partition=false)
  -   trusty: true(ref_target=celadon_apl)
  +   trusty: false`

source build/envsetup.sh
device/intel/mixins/mixin-update
```

# V. Build
```
source build/envsetup.sh
lunch celadon_tablet-userdebug
make SPARSE_IMG=true flashfiles -j8
```
  
# VI. Deployment
## SATA deployment was tested and works well from AOSP installation USB.

1. Copy the image you built on USB flash
```
LOCAL_OASP_PATH=/media/data/Work/vbox/02.projects/020.Andriod-2019/012.aosb-bsp
DESKTOP_IP=172.16.10.113
scp ~/celadon.dev/out/target/product/celadon_tablet/celadon_tablet-flashfiles-eng.honza.zip "$DESKTOP_IP:$LOCAL_OASP_PATH"

lsblk
```

2. Define system variable SD providing block device node path.
[ Example: SD=/dev/sdb ]
```
sudo mkfs.vfat "$SD" -I
      
sudo mount "$SD" /media/flash
cd /media/flash

sudo rm -rf LOST.DIR *
sudo unzip "$LOCAL_OASP_PATH"/celadon_tablet-flashfiles-eng.honza.zip
```

3. copy efi boot loader on the USB flash
```
cd $LOCAL_OASP_PATH
wget https://raw.githubusercontent.com/tianocore/edk2/UDK2018/ShellBinPkg/UefiShell/X64/Shell.efi

mkdir efi/boot -p
cd efi/boot
sudo cp $LOCAL_OASP_PATH/Shell.efi BOOTX64.efi

cd ~
sudo umount flash
sync
```

4. connect sata storage to the target device
5. boot the target from the USB flash
6. the system will install Android 10 on SATA automatically

# VII. ADB over USB
ADB gives you a community-maintained default set of udev rules for all Android devices.

## Install adb package :
```
sudo apt install adb
```

## Make sure that you are member of the plugdev group
```
id
  uid=1000(honza) gid=1000(honza) groups=1000(honza),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),113(lpadmin),128(sambashare)
```
  => OK
  else :
  ```
    sudo usermod -aG plugdev $LOGNAME
  ```

## HW setup
[ Note: *This expects QA50 and QEV2 as hw configuration. Instructions for other hw setup can slightly differ.* ]
   
1. setup USB client in BIOS :
  - Enter BIOS Setup
  - Open **Chipset** menu
  - Select  **South Cluster Configuration**
  - Open **USB Configuration**
  - Set **XDCI Support" as "[PCI Mode]**

2. set USB slave mode - set jumper X37 to position 1-2 on QEV2
3. connect USB to target
  
4. root access to the target
```
adb devices -l
  List of devices attached
  000003382173           device usb:3-3 product:celadon_tablet model:AOSP_on_Intel_Platform device:celadon_tablet transport_id:1
```

5. connect the device :
```
adb root
adb shell
```

[ Note: *more informations about adb at https://developer.android.com/studio/command-line/adb* ]

# VIII. Networking
## Wifi
The wlan0 interface is available after start.

## Bluetooth
Bluetooth is enabled and available after start by default.

## Ethernet
The interface is intentionally disabled by default. The eth0 interface can be enabled manually from adb shell by :
```
insmod vendor/lib/modules/dca.ko
insmod vendor/lib/modules/igb.ko
```

Connection details :
```
ip link
  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
  2: ip_vti0@NONE: <NOARP> mtu 1480 qdisc noop state DOWN mode DEFAULT group default qlen 1000
      link/ipip 0.0.0.0 brd 0.0.0.0
  3: ip6_vti0@NONE: <NOARP> mtu 1364 qdisc noop state DOWN mode DEFAULT group default qlen 1000
      link/tunnel6 :: brd ::
  4: sit0@NONE: <NOARP> mtu 1480 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/sit 0.0.0.0 brd 0.0.0.0
  5: ip6tnl0@NONE: <NOARP> mtu 1452 qdisc noop state DOWN mode DEFAULT group default qlen 1000
      link/tunnel6 :: brd ::
  6: wlan0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
      link/ether 7c:5c:f8:XX:XX:XX brd ff:ff:ff:ff:ff:ff
  8: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
      link/ether 00:13:95:XX:XX:XX brd ff:ff:ff:ff:ff:ff

ifconfig
  lo        Link encap:Local Loopback  
            inet addr:127.0.0.1  Mask:255.0.0.0 
            inet6 addr: ::1/128 Scope: Host
            UP LOOPBACK RUNNING  MTU:65536  Metric:1
            RX packets:0 errors:0 dropped:0 overruns:0 frame:0 
            TX packets:0 errors:0 dropped:0 overruns:0 carrier:0 
            collisions:0 txqueuelen:1000 
            RX bytes:0 TX bytes:0 

  eth0      Link encap:Ethernet  HWaddr 00:13:95:XX:XX:XX  Driver igb
            inet addr:XXX.XXX.XXX.112  Bcast:XXX.XXX.XXX.255  Mask:255.255.255.0
            inet6 addr: fe80::ab3a:ebcb:df16:5f21/64 Scope: Link
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:1811 errors:0 dropped:0 overruns:0 frame:0 
            TX packets:1712 errors:0 dropped:0 overruns:0 carrier:0 
            collisions:0 txqueuelen:1000 
            RX bytes:1320661 TX bytes:327714 
            Memory:81300000-8131ffff
```
